package uz.pdp.appgraphql.mapper;

import org.mapstruct.Mapper;
import uz.pdp.appgraphql.entity.Post;
import uz.pdp.appgraphql.payload.PostDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PostMapper {


    List<PostDTO> toPostDTOList(List<Post> posts);
}
