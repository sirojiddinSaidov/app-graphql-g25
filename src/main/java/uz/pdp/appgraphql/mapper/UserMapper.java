package uz.pdp.appgraphql.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import uz.pdp.appgraphql.entity.User;
import uz.pdp.appgraphql.payload.UserAddDTO;
import uz.pdp.appgraphql.payload.UserDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User toUser(UserAddDTO userAddDTO);

    @Mapping(target = "followers", ignore = true)
    UserDTO toUserDTO(User user);

    List<UserDTO> toUserDTOList(List<User> all);

}
