package uz.pdp.appgraphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppGraphqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppGraphqlApplication.class, args);
    }

}
