package uz.pdp.appgraphql.payload;

import lombok.Data;

@Data
public class CommentDTO {

    private Integer id;

    private String text;
}
