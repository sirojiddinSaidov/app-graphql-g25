package uz.pdp.appgraphql.payload;

import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import lombok.Data;
import uz.pdp.appgraphql.entity.User;

import java.time.LocalDate;
import java.util.List;

@Data
public class UserDTO {

    private Integer id;

    private String name;

    private LocalDate birthDate;

    private String address;

    private List<UserDTO> followers;

    private List<PostDTO> posts;

}
