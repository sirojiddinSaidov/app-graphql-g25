package uz.pdp.appgraphql.payload;

import lombok.Data;
import uz.pdp.appgraphql.entity.Comment;

import java.util.List;

@Data
public class PostDTO {

    private Integer id;

    private String title;

    private String body;

    private List<CommentDTO> comments;
}
