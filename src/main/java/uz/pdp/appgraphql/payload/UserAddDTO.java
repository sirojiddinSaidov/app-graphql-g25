package uz.pdp.appgraphql.payload;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UserAddDTO {

    private String name;

    private LocalDate birthDate;

    private String address;
}
