package uz.pdp.appgraphql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appgraphql.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
}