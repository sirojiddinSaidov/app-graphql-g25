package uz.pdp.appgraphql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appgraphql.entity.Post;
import uz.pdp.appgraphql.entity.User;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Integer> {
    List<Post> findAllByUser(User user);

    List<Post> findAllByUserId(Integer userId);
}