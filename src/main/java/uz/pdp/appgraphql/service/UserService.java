package uz.pdp.appgraphql.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.appgraphql.entity.Post;
import uz.pdp.appgraphql.entity.User;
import uz.pdp.appgraphql.mapper.PostMapper;
import uz.pdp.appgraphql.mapper.UserMapper;
import uz.pdp.appgraphql.payload.PostDTO;
import uz.pdp.appgraphql.payload.UserAddDTO;
import uz.pdp.appgraphql.payload.UserDTO;
import uz.pdp.appgraphql.repository.PostRepository;
import uz.pdp.appgraphql.repository.UserRepository;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final UserMapper userMapper;
    private final PostMapper postMapper;

    public UserDTO add(UserAddDTO userAddDTO) {
        User user = userMapper.toUser(userAddDTO);
        userRepository.save(user);
        return userMapper.toUserDTO(user);
    }

    public List<UserDTO> list() {
        return userMapper.toUserDTOList(userRepository.findAll());
    }

    public UserDTO getUserInfo(Integer id) {
        User user = userRepository.findById(id).orElseThrow();
        List<Post> posts = postRepository.findAllByUser(user);
        List<User> followers = user.getFollowers();
        List<PostDTO> postDTOS = postMapper.toPostDTOList(posts);

        UserDTO userDTO = userMapper.toUserDTO(user);
        userDTO.setFollowers(userMapper.toUserDTOList(followers));
        userDTO.setPosts(postDTOS);
        return userDTO;
    }
}
