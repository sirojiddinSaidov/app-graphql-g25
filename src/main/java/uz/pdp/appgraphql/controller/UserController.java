package uz.pdp.appgraphql.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appgraphql.exceptions.AppGraphqlException;
import uz.pdp.appgraphql.payload.UserAddDTO;
import uz.pdp.appgraphql.payload.UserDTO;
import uz.pdp.appgraphql.service.UserService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @PostMapping("/user/add")
    public UserDTO addUser(@RequestBody UserAddDTO userAddDTO) {
        return userService.add(userAddDTO);
    }

    @MutationMapping
    public UserDTO createUser(@Argument UserAddDTO userAddDTO) {
        return userService.add(userAddDTO);
    }

    @GetMapping("/users")
    public List<UserDTO> list2() {
        return userService.list();
    }

    @QueryMapping("users")
    public List<UserDTO> list() {
        return userService.list();
    }

    @QueryMapping("test")
    public UserDTO test() {
        throw new AppGraphqlException("OKa test");
    }


    @QueryMapping
    public UserDTO allInOneUser(@Argument("uroq") Integer id) {
        return userService.getUserInfo(id);
    }
}
