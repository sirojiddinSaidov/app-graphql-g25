package uz.pdp.appgraphql.exceptions;

import lombok.Getter;
import org.springframework.graphql.execution.ErrorType;
import org.springframework.web.bind.annotation.GetMapping;

@Getter
public class AppGraphqlException extends RuntimeException {
    private ErrorType status = ErrorType.BAD_REQUEST;

    public AppGraphqlException(String message) {
        super(message);
    }

    public AppGraphqlException(String message, ErrorType status) {
        super(message);
        this.status = status;
    }
}
